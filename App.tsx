import React, { useEffect } from 'react';
import * as Notifications from 'expo-notifications';

import {
  useFonts,
  Jost_400Regular,
  Jost_600SemiBold,
} from '@expo-google-fonts/jost';

import AppLoading from 'expo-app-loading';
import Routes from './src/routes';
import { IPlant } from './src/services/interface';

export default function App() {
  const [fontsLoaded] = useFonts({
    Jost_400Regular,
    Jost_600SemiBold,
  });

  useEffect(() => {
    const subscription = Notifications.addNotificationReceivedListener(
      async (notification) => {
        const data = notification.request.content.data.plant as IPlant;
        console.log(data);
      },
    );

    // Notificantion Function Examples
    async function notificationScheduled() {
      const data = await Notifications.getAllScheduledNotificationsAsync();
      console.log('##### Notificações Agendadas #####');
      console.log(data);
    }

    async function cancelAllNotifications() {
      await Notifications.cancelAllScheduledNotificationsAsync();
      console.log('##### Todas as notificações foram canceladas #####');

      const data = await Notifications.getAllScheduledNotificationsAsync();
      console.log(data);
    }

    return () => subscription.remove();
  }, []);

  if (!fontsLoaded) return <AppLoading />;

  return <Routes />;
}
