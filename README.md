# 1- What I learned?

### - Typescript
### - React Navigation
### - React Native Gesture Handler
### - React Native SVG
### - Expo Async Storage: save data in your device
https://docs.expo.io/versions/latest/sdk/async-storage/
### - Expo DateTimePicker
https://docs.expo.io/versions/latest/sdk/date-time-picker/
### - Tab navigation
https://reactnavigation.org/docs/tab-based-navigation/
### - Expo Notifications
https://docs.expo.io/versions/latest/sdk/notifications/
### - JSON Serve to fake API


# 2- Important Dependencies

### - Expo App Loading

### - Lottie: Animation in React Native
reference:
https://airbnb.io/lottie/#/

### - Expo Vector Icons
references:
NPM: https://www.npmjs.com/package/@expo/vector-icons
Documentation: https://docs.expo.io/guides/icons/
Find a icon: https://icons.expo.fyi/

### - Expo Google Fonts
reference:
Github: https://github.com/expo/google-fonts


## 3 - Import Tips

### - Broken Expo App Broken
- Clean up the cache and data of your Expo App inside your Device configuration
- Clean Expo environment in your PC with 'yarn start-clean' (command in package.json) 