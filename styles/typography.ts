import colors from './colors';
import fonts from './fonts';

export const title = {
  color: colors.heading,
  fontFamily: fonts.heading,
};

export const content = {
  color: colors.heading,
  fontFamily: fonts.text,
};

export const input = {
  color: colors.heading,
  borderColor: colors.gray,
};
