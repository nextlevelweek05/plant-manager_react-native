import vectorIcons from '@expo/vector-icons';
import { Entypo, Feather, FontAwesome } from '@expo/vector-icons';

// references:
// NPM: https://www.npmjs.com/package/@expo/vector-icons
// Documentation: https://docs.expo.io/guides/icons/
// Find a icon: https://icons.expo.fyi/

export default vectorIcons;
