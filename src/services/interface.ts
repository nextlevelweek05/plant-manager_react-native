export interface IPlantsEnvironment {
  key: string;
  title: string;
}

export interface IPlantsWaterFrequency {
  key: string;
  title: string;
}

export type Periods = 'day' | 'week' | 'month';

export interface IFrequency {
  times: number;
  repeat_every: Periods;
}

export interface IPlant {
  id: number;
  name: string;
  about: string;
  water_tips: string;
  photo: string;
  environments: string[];
  frequency: IFrequency;
  hour: string;
  dateTimeNotification: Date;
  notificationId: string;
}
