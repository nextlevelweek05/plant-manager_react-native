import axios from 'axios';

const api = axios.create({
  // windows:
  // ipconfig
  // IPv4 Address
  baseURL: 'http://192.168.0.169:3333',
});

export default api;
