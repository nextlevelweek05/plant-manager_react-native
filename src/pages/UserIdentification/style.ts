import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';

import { title, input } from '../../../styles/typography';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  content: {
    flex: 1,
    width: '100%',
  },
  form: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 54,
    alignItems: 'center',
  },
  title: {
    ...title,
    fontSize: 24,
    lineHeight: 32,
    marginTop: 20,
    textAlign: 'center',
  },
  emoji: {
    fontSize: 44,
  },
  input: {
    ...input,
    borderBottomWidth: 1,
    width: '100%',
    fontSize: 18,
    marginTop: 50,
    padding: 10,
    textAlign: 'center',
  },
  inputFocus: {
    borderColor: colors.green,
    borderBottomWidth: 2,
  },
  header: {
    alignItems: 'center',
  },
  footer: {
    width: '100%',
    marginTop: 40,
    paddingHorizontal: 20,
  },
});
