import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/core';
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Button from '../../components/Button';

import style from './style';
import { setItemStorage } from '../../storage/helpers';
import { IParams } from '../Confirmation/interface';

const UserIdentification = () => {
  const [isFocused, setIsFocused] = useState(false);
  const [isFilled, setIsFilled] = useState(false);
  const [name, setName] = useState<string>('');

  const navigation = useNavigation();

  const handleInputChange = (value: string) => {
    setIsFilled(!!value);
    setName(value);
  };

  const handleConfirm = async () => {
    await setItemStorage('user', name);
    navigation.navigate('Confirmation', {
      title: `Prontinho ${name}!`,
      subtitle:
        'Agora vamos começar a cuidar das suas plantinhas com muito cuidado.',
      buttonTitle: 'Continuar',
      icon: 'plant',
      nextScreen: 'PlantSelect',
    } as IParams);
  };

  const handleOnExitInput = () => {
    setIsFocused(false);
    Keyboard.dismiss();
  };

  return (
    <SafeAreaView style={style.container}>
      <KeyboardAvoidingView
        style={style.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      >
        <TouchableWithoutFeedback onPress={handleOnExitInput}>
          <View style={style.content}>
            <View style={style.form}>
              <View style={style.header}>
                <Text style={style.emoji}>
                  {/* window + . */}
                  {isFilled ? '😃' : '😁'}
                </Text>

                <Text style={style.title}>
                  Como podemos {'\n'}
                  chamar você?
                </Text>
              </View>

              <TextInput
                style={[
                  style.input,
                  (isFocused || isFilled) && style.inputFocus,
                ]}
                placeholder="Digite seu nome"
                onBlur={() => setIsFocused(false)}
                onFocus={() => setIsFocused(true)}
                onChangeText={handleInputChange}
              />

              <View style={style.footer}>
                <Button disabled={!isFilled} onPress={handleConfirm}>
                  Confirmar
                </Button>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default UserIdentification;
