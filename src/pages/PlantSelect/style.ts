import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';
import { title, content } from '../../../styles/typography';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
  },
  header: {
    paddingHorizontal: 32,
  },
  title: {
    ...title,
    fontSize: 17,
    lineHeight: 20,
    marginTop: 15,
  },
  subTitle: {
    ...content,
    fontSize: 17,
    lineHeight: 20,
  },
  environmentList: {
    height: 40,
    justifyContent: 'center',
    paddingBottom: 5,
    marginLeft: 32,
    paddingRight: 64,
    marginVertical: 32,
  },
  plants: {
    flex: 1,
    paddingHorizontal: 32,
    justifyContent: 'center',
  },
  plantList: {},
  endItens: {
    flex: 1,
    height: 5,
    borderRadius: 5,
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: colors.green_light,
  },
});
