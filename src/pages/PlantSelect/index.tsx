import React, { useEffect, useState, Fragment } from 'react';
import { Text, View, FlatList, ActivityIndicator } from 'react-native';

import EnvironmentButton from '../../components/EnvironmentButton';
import Header from '../../components/Header';
import PlantCardPrimary from '../../components/PlantCardPrimary';
import Loading from '../../components/Loading';

import api from '../../services/api';
import { IPlant, IPlantsEnvironment } from '../../services/interface';

import style from './style';
import colors from '../../../styles/colors';
import { useNavigation } from '@react-navigation/core';

const PlantSelect = () => {
  const [loading, setLoading] = useState(true);

  const [page, setPage] = useState(1);
  const [loadingMore, setLoadingMore] = useState(false);
  const [hasLoadingMore, setHasLoadingMore] = useState(true);

  const [environments, setEnvironments] = useState<IPlantsEnvironment[]>([]);
  const [environmentSelected, setEnvironmentSelected] = useState('all');

  const [plants, setPlants] = useState<IPlant[]>([]);

  const navigation = useNavigation();

  useEffect(() => {
    async function fetchEnvironment() {
      const { data } = await api.get<IPlantsEnvironment[]>(
        'plants_environments?_sort=title&_order=asc',
      );
      setEnvironments([{ key: 'all', title: 'Todos' }, ...data]);
    }

    fetchEnvironment();
  }, []);

  useEffect(() => {
    setLoading(true);
    fetchPlants();
  }, [environmentSelected]);

  async function fetchPlants() {
    const limit = 8;
    const filter =
      environmentSelected !== 'all'
        ? `&environments_like=${environmentSelected}`
        : '';

    const { data } = await api.get<IPlant[]>(
      `plants?_sort=name&_order=asc&_page=${page}&_limit=${limit}${filter}`,
    );

    if (!data) return setLoading(true);

    if (data.length < limit) setHasLoadingMore(false);

    if (page > 1) setPlants((oldValues) => [...oldValues, ...data]);
    else setPlants(data);

    setLoading(false);
    setLoadingMore(false);
  }

  const handleEnvironmentChange = (key: string) => {
    setPage(1);
    setEnvironmentSelected(key);
  };

  const handleFetchMore = (distance: number) => {
    if (distance < 1 || !hasLoadingMore) return;

    setLoadingMore(true);
    setPage((oldValue) => oldValue + 1);
    fetchPlants();
  };

  const handlePlantSelect = (plant: IPlant) => {
    navigation.navigate('PlantSave', { plant });
  };

  return (
    <View style={style.container}>
      <View style={style.header}>
        <Header />
        <Text style={style.title}>Em qual ambiente</Text>
        <Text style={style.subTitle}>você quer colocar sua planta?</Text>
      </View>

      {loading ? (
        <Loading />
      ) : (
        <Fragment>
          <View>
            <FlatList
              contentContainerStyle={style.environmentList}
              horizontal
              showsHorizontalScrollIndicator={false}
              data={environments}
              keyExtractor={(item) => String(item.key)}
              renderItem={({ item }) => (
                <EnvironmentButton
                  onPress={() => handleEnvironmentChange(item.key)}
                  title={item.title}
                  active={environmentSelected === item.key}
                />
              )}
            />
          </View>

          <View style={style.plants}>
            <FlatList
              contentContainerStyle={style.plantList}
              showsVerticalScrollIndicator={false}
              numColumns={2}
              onEndReachedThreshold={0.1}
              onEndReached={({ distanceFromEnd }) =>
                handleFetchMore(distanceFromEnd)
              }
              ListFooterComponent={
                loadingMore ? (
                  <ActivityIndicator color={colors.green} />
                ) : (
                  <View style={style.endItens} />
                )
              }
              data={plants}
              keyExtractor={(item) => String(item.id)}
              renderItem={({ item }) => (
                <PlantCardPrimary
                  data={{ name: item.name, photo: item.photo }}
                  onPress={() => handlePlantSelect(item)}
                />
              )}
            />
          </View>
        </Fragment>
      )}
    </View>
  );
};

export default PlantSelect;
