import React, { useEffect, useState } from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/core';

import Button from '../../components/Button';
import style from './style';
import { Emojis, IParams } from './interface';
import { getItemStorage } from '../../storage/helpers';
import { useRoute } from '@react-navigation/native';

const Confirmation = () => {
  const navigation = useNavigation();
  const routes = useRoute();

  const {
    title,
    subtitle,
    buttonTitle,
    icon,
    nextScreen,
  } = routes.params as IParams;

  const [userName, setUserName] = useState<string>();

  useEffect(() => {
    const loadStoreageUserName = async () => {
      const user = await getItemStorage('user');
      setUserName(user || '');
    };
    loadStoreageUserName();
  }, []);

  return (
    <SafeAreaView style={style.container}>
      <View style={style.content}>
        <Text style={style.emoji}>{Emojis[icon]}</Text>

        <Text style={style.title}>{title}</Text>

        <Text style={style.subtitle}>{subtitle}</Text>

        <View style={style.footer}>
          <Button onPress={() => navigation.navigate(nextScreen)}>
            {buttonTitle}
          </Button>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Confirmation;
