export enum Emojis {
  hug = '🤗',
  smile = '😃',
  plant = '🌱',
}

export interface IParams {
  title: string;
  subtitle: string;
  buttonTitle: string;
  icon: keyof typeof Emojis;
  nextScreen: string;
}
