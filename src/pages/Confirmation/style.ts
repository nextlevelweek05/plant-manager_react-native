import { StyleSheet } from 'react-native';

import { title, content } from '../../../styles/typography';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  content: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    padding: 30,
  },
  title: {
    ...title,
    fontSize: 20,
    textAlign: 'center',
    lineHeight: 38,
    marginTop: 15,
  },
  subtitle: {
    ...content,
    textAlign: 'center',
    fontSize: 17,
    paddingVertical: 20,
  },
  emoji: {
    fontSize: 96,
  },
  footer: {
    width: '100%',
    paddingHorizontal: 50,
    marginTop: 20,
  },
});
