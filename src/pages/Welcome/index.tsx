import React, { useEffect, useState } from 'react';
import { SafeAreaView, Text, Image, View } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import AsyncStorage from '@react-native-async-storage/async-storage';

import style from './style';
import wateringImg from '../../assets/watering.png';

import Button from '../../components/Button';
import { Feather } from '@expo/vector-icons';
import Loading from '../../components/Loading';
import { getItemStorage } from '../../storage/helpers';

const Welcome = () => {
  const navigation = useNavigation();

  const [userName, setUserName] = useState<string>();
  const [loadedName, setLoadedName] = useState(false);

  useEffect(() => {
    const loadStoreageUserName = async () => {
      const user = await getItemStorage('user');
      setUserName(user || '');
      setLoadedName(true);
    };
    loadStoreageUserName();
  }, []);

  if (userName) navigation.navigate('PlantSelect');

  if (!loadedName) return <Loading />;

  return (
    <SafeAreaView style={style.container}>
      <View style={style.wrapper}>
        <Text style={style.title}>
          Gerencie {'\n'}
          suas plantas de {'\n'}
          forma fácil!
        </Text>

        <Image source={wateringImg} style={style.image} resizeMode="contain" />

        <Text style={style.subtitle}>
          Não esqueça mais de regar suas plantas. Nós cuidamos de lembrar você
          sempre que precisar.
        </Text>

        <Button onPress={() => navigation.navigate('UserIdentification')}>
          <Feather name="chevron-right" style={style.icon} />
        </Button>
      </View>
    </SafeAreaView>
  );
};

export default Welcome;
