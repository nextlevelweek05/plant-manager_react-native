import { StyleSheet, Dimensions } from 'react-native';

import { title, content } from '../../../styles/typography';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: 20,
  },
  title: {
    ...title,
    fontSize: 28,
    lineHeight: 34,
    textAlign: 'center',
    marginTop: 38,
  },
  subtitle: {
    ...content,
    textAlign: 'center',
    fontSize: 18,
    paddingHorizontal: 20,
  },
  image: {
    width: Dimensions.get('window').width * 0.7,
  },
  icon: {
    fontSize: 24,
  },
});
