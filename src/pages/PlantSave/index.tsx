import React, { Fragment, useState } from 'react';
import {
  Alert,
  Text,
  View,
  Image,
  ScrollView,
  Platform,
  TouchableOpacity,
} from 'react-native';
import DateTimePicker, { Event } from '@react-native-community/datetimepicker';
import { SvgFromUri } from 'react-native-svg';
import { useRoute } from '@react-navigation/core';

import Button from '../../components/Button';

import waterdropImg from '../../assets/waterdrop.png';

import style from './style';
import { IPlant } from '../../services/interface';
import { format, isBefore } from 'date-fns';
import { savePlant } from '../../storage/plant';
import { useNavigation } from '@react-navigation/native';
import { IParams } from '../Confirmation/interface';

const PlantSave = () => {
  const [selectedDatetime, setSelectedDatetime] = useState(new Date());
  const [showDatePicker, setShowDatePicker] = useState(Platform.OS === 'ios');

  const route = useRoute();
  const navigation = useNavigation();
  const { plant } = route.params as { plant: IPlant };

  const handleChangeTime = (_: Event, dateTime: Date | undefined) => {
    if (Platform.OS === 'android') setShowDatePicker((oldState) => !oldState);

    // if (dateTime && isBefore(dateTime, new Date())) {
    //   setSelectedDatetime(new Date());
    //   return Alert.alert('Escolha uma hora maior que agora! ⌚');
    // }

    if (dateTime) setSelectedDatetime(dateTime);
  };

  const handleSave = async () => {
    try {
      await savePlant({
        ...plant,
        dateTimeNotification: selectedDatetime,
      });
      // Alert.alert('Planta Cadastrada com Sucesso! 😉');

      navigation.navigate('Confirmation', {
        title: `Tudo certo!`,
        subtitle:
          'Fique tranquilo que sempre vamos lembrar você de cuidar da sua plantinha com muito carinho.',
        buttonTitle: 'Muito Obrigado',
        icon: 'hug',
        nextScreen: 'MyPlants',
      } as IParams);
    } catch (e) {
      Alert.alert('Desculpe, mas não foi possível salvar... 😅');
    }
  };

  return (
    <View style={style.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={style.plantInfo}>
          <SvgFromUri uri={plant.photo} height={150} width={150} />

          <Text style={style.plantName}>{plant.name}</Text>
          <Text style={style.plantAbout}>{plant.about}</Text>
        </View>

        <View style={style.controller}>
          <View style={style.tipContainer}>
            <Image source={waterdropImg} style={style.tipImage} />
            <Text style={style.tipText}>{plant.water_tips}</Text>
          </View>

          <Text style={style.alertLabel}>
            Escolha o melhor horário para ser lembrado:
          </Text>

          {showDatePicker && (
            <DateTimePicker
              value={selectedDatetime}
              mode="time"
              display="spinner"
              onChange={handleChangeTime}
              minimumDate={new Date()}
            />
          )}

          <Text style={style.dateTimePickerTextSelected}>
            {format(selectedDatetime, 'HH:mm') || 'Selecione um horário 👇'}
          </Text>

          {Platform.OS === 'android' && (
            <TouchableOpacity
              style={style.dateTimePickerButton}
              onPress={() => setShowDatePicker((oldState) => !oldState)}
            >
              <Text style={style.dateTimePickerText}>Mudar horário</Text>
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>

      <View style={style.controller}>
        <Button onPress={handleSave}>Cadastrar planta</Button>
      </View>
    </View>
  );
};

export default PlantSave;
