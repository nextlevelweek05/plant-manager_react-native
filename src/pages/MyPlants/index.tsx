import React, { Fragment, useEffect, useState } from 'react';
import { View, Text, Image, Alert } from 'react-native';

import waterdropImg from '../../assets/waterdrop.png';

import style from './style';
import { IPlant } from '../../services/interface';
import { loadPlants, loadPlantSorted, removePlant } from '../../storage/plant';
import { formatDistance } from 'date-fns';
import { pt } from 'date-fns/locale';

import Header from '../../components/Header';
import { FlatList } from 'react-native-gesture-handler';
import PlantCardSecondary from '../../components/PlantCardSecondary';
import Loading from '../../components/Loading';
import AsyncStorage from '@react-native-async-storage/async-storage';

const MyPlants = () => {
  const [loading, setLoading] = useState(true);
  const [myPlants, setMyPlants] = useState<IPlant[]>([]);
  const [nextWatered, setNextWatered] = useState<string>();

  useEffect(() => {
    async function loadStorageData() {
      const plantsStorage = await loadPlants();

      if (plantsStorage.length) {
        const nextTime = formatDistance(
          new Date(plantsStorage[0].dateTimeNotification).getTime(),
          new Date().getTime(),
          { locale: pt },
        );
        setNextWatered(
          `Não esqueça de regar a ${plantsStorage[0].name} em ${nextTime}...`,
        );
        setMyPlants(plantsStorage);
      }

      setLoading(false);
    }
    loadStorageData();
  }, []);

  const handleRemove = (plant: IPlant) => {
    Alert.alert('Remover', `Deseja remover a ${plant.name}?`, [
      {
        text: 'Não 👎',
        style: 'cancel',
      },
      {
        text: 'Sim 👍',
        onPress: async () => {
          try {
            await removePlant(plant.id);
            setMyPlants((oldData) =>
              oldData.filter((item) => item.id !== plant.id),
            );
          } catch (error) {
            Alert.alert('Não foi possível remover! 🙄');
          }
        },
      },
    ]);
  };

  if (loading) return <Loading />;

  return (
    <View style={style.container}>
      <Header />

      {myPlants.length ? (
        <Fragment>
          <View style={style.spotlight}>
            <Image source={waterdropImg} style={style.spotlightImage} />
            <Text style={style.spotlightText}>{nextWatered}</Text>
          </View>

          <View style={style.plants}>
            <Text style={style.plantsTitle}>Próximas regadas</Text>

            <FlatList
              data={myPlants}
              keyExtractor={(item) => String(item.id)}
              renderItem={({ item }) => (
                <PlantCardSecondary
                  data={item}
                  handleRemove={() => handleRemove(item)}
                />
              )}
              showsVerticalScrollIndicator={false}
            />
          </View>
        </Fragment>
      ) : (
        <View style={style.plants}>
          <Text style={style.plantsTitle}>
            Ainda sem plantas cadastradas...
          </Text>
        </View>
      )}
    </View>
  );
};

export default MyPlants;
