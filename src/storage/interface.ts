import { IPlant } from '../services/interface';

export type ItemsType = 'plants' | 'user';

export interface IConfig {
  name: string;
  items: {
    [key in ItemsType]: string;
  };
}

export interface IStoragePlant {
  [id: string]: {
    data: IPlant;
    notificationId: string;
  };
}
