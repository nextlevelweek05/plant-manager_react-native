import { IConfig } from './interface';

const config: IConfig = {
  name: '@plantmanager-reactnative',
  items: {
    plants: 'plants',
    user: 'user',
  },
};

export default config;
