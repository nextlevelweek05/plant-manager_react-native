import { IPlant } from '../services/interface';
import { IStoragePlant } from './interface';
import * as Notifications from 'expo-notifications';

import { getItemStorage, setItemStorage } from './helpers';
import { format } from 'date-fns';

const getStoragePlants = async (): Promise<IStoragePlant> => {
  const data = await getItemStorage('plants');
  return data ? (JSON.parse(data) as IStoragePlant) : {};
};

export async function savePlant(plant: IPlant): Promise<void> {
  try {
    const nextTime = new Date(plant.dateTimeNotification);
    const now = new Date();

    const { times, repeat_every } = plant.frequency;

    if (repeat_every === 'week') {
      const interval = Math.trunc(7 / times);
      nextTime.setDate(now.getDate() + interval);
    } else {
      nextTime.setDate(nextTime.getDate() + 1);
    }

    const seconds = Math.abs(
      Math.ceil((now.getTime() - nextTime.getTime()) / 1000),
    );

    const notificationId = await Notifications.scheduleNotificationAsync({
      content: {
        title: 'Tem plantinha para regar 🌱',
        body: `Está na hora de cuidar da ${plant.name}!`,
        sound: true,
        priority: Notifications.AndroidNotificationPriority.HIGH,
        data: {
          plant,
        },
      },
      trigger: {
        seconds: seconds < 60 ? 60 : seconds,
        repeats: true,
      },
    });

    const oldPlants = await getStoragePlants();

    const newPlant = {
      [plant.id]: {
        data: plant,
        notificationId,
      },
    } as IStoragePlant;

    await setItemStorage('plants', { ...newPlant, ...oldPlants });
  } catch (e) {
    throw new Error(e);
  }
}

export async function loadPlants(): Promise<IPlant[]> {
  try {
    const plants = await getStoragePlants();

    return Object.keys(plants).map((plantKey) => ({
      ...plants[plantKey].data,
      hour: format(
        new Date(plants[plantKey].data.dateTimeNotification),
        'HH:mm',
      ),
    }));
  } catch (e) {
    throw new Error(e);
  }
}

export async function loadPlantSorted(): Promise<IPlant[]> {
  const plants = await loadPlants();

  return plants.sort((a, b) =>
    Math.floor(
      new Date(a.dateTimeNotification).getTime() / 1000 -
        Math.floor(new Date(b.dateTimeNotification).getTime() / 1000),
    ),
  );
}

export async function removePlant(id: number): Promise<void> {
  const plants = await getStoragePlants();

  await Notifications.cancelScheduledNotificationAsync(
    plants[id].notificationId,
  );
  delete plants[id];

  await setItemStorage('plants', plants);
}
