import AsyncStorage from '@react-native-async-storage/async-storage';
import config from './config';
import { ItemsType } from './interface';

export const getStorageName = (item: ItemsType): string =>
  config.items[item] ? `${config.name}:${config.items[item]}` : '';

export const getItemStorage = async (item: ItemsType) =>
  await AsyncStorage.getItem(getStorageName(item));

export const setItemStorage = async (item: ItemsType, value: unknown) =>
  await AsyncStorage.setItem(
    getStorageName(item),
    typeof value === 'string' ? value : JSON.stringify(value),
  );
