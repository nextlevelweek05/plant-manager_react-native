import React, { useEffect, useState } from 'react';
import { Text, Image, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import style from './style';
import profileImg from '../../assets/user01.png';
import { getItemStorage } from '../../storage/helpers';

const Header = () => {
  const [userName, setUserName] = useState<string>();

  useEffect(() => {
    const loadStoreageUserName = async () => {
      const user = await getItemStorage('user');
      setUserName(user || '');
    };
    loadStoreageUserName();
  }, []);

  return (
    <View style={style.container}>
      <View>
        <Text style={style.greeting}>Olá,</Text>
        <Text style={style.userName}>{userName}</Text>
      </View>

      <Image source={profileImg} style={style.image} />
    </View>
  );
};

export default Header;
