import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import colors from '../../../styles/colors';
import { title, content } from '../../../styles/typography';

export default StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
    marginTop: getStatusBarHeight(),
  },
  image: {
    width: 64,
    height: 64,
    borderRadius: 40,
  },
  greeting: {
    ...content,
    fontSize: 32,
  },
  userName: {
    ...title,
    fontSize: 32,
    lineHeight: 40,
  },
});
