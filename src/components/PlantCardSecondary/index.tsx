import { Feather } from '@expo/vector-icons';
import React from 'react';
import { Text, View, Animated } from 'react-native';
import { RectButton, Swipeable } from 'react-native-gesture-handler';
import { SvgFromUri } from 'react-native-svg';
import colors from '../../../styles/colors';

import { IPlantCardSecondary } from './interface';
import style from './style';

const PlantCardSecondary = ({
  data,
  handleRemove,
  ...rest
}: IPlantCardSecondary) => {
  return (
    <Swipeable
      overshootRight={false}
      renderRightActions={() => (
        <Animated.View>
          <View style={style.buttonWrapper}>
            <RectButton style={style.buttonRemove} onPress={handleRemove}>
              <Feather name="trash" size={32} color={colors.white} />
            </RectButton>
          </View>
        </Animated.View>
      )}
    >
      <RectButton style={style.container} {...rest}>
        <SvgFromUri uri={data.photo} width={70} height={70} />
        <Text style={style.title}>{data.name}</Text>

        <View style={style.details}>
          <Text style={style.timeLabel}>Reagar ás</Text>
          <Text style={style.time}>{data.hour}</Text>
        </View>
      </RectButton>
    </Swipeable>
  );
};

export default PlantCardSecondary;
