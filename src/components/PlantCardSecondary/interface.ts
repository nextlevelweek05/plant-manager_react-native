import { RectButtonProps } from 'react-native-gesture-handler';

export interface IPlantData {
  name: string;
  photo: string;
  hour: string;
}

export interface IPlantCardSecondary extends RectButtonProps {
  data: IPlantData;
  handleRemove: () => void;
}
