import React from 'react';
import { Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import { SvgFromUri } from 'react-native-svg';

import { IPlantCardPrimary } from './interface';
import style from './style';

const PlantCardPrimary = ({ data, ...rest }: IPlantCardPrimary) => {
  return (
    <RectButton style={style.container} {...rest}>
      <SvgFromUri uri={data.photo} width={70} height={70} />
      <Text style={style.text}>{data.name}</Text>
    </RectButton>
  );
};

export default PlantCardPrimary;
