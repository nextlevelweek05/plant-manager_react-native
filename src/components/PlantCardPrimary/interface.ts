import { RectButtonProps } from 'react-native-gesture-handler';

export interface IPlantData {
  name: string;
  photo: string;
}

export interface IPlantCardPrimary extends RectButtonProps {
  data: IPlantData;
}
