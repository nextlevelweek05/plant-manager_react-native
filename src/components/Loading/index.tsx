import React from 'react';
import { View } from 'react-native';
import LottieView from 'lottie-react-native';

import loadingAnimation from '../../assets/load.json';
import style from './style';

const Loading = () => {
  return (
    <View style={style.container}>
      <LottieView
        source={loadingAnimation}
        autoPlay
        loop
        style={style.animation}
      />
    </View>
  );
};

export default Loading;
