import React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';

import style from './style';
import { IButton } from './interface';

const Button = ({ children, ...rest }: IButton) => {
  return (
    <TouchableOpacity
      style={[style.button, rest.disabled && style.disabled]}
      activeOpacity={0.7}
      {...rest}
    >
      <Text style={style.buttonText}>{children}</Text>
    </TouchableOpacity>
  );
};

export default Button;
