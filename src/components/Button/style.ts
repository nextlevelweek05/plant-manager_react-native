import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';

export default StyleSheet.create({
  button: {
    backgroundColor: colors.green,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    height: 56,
    minWidth: 56,
  },
  disabled: {
    backgroundColor: colors.gray,
    opacity: 0.8,
  },
  buttonText: {
    color: colors.white,
    fontSize: 16,
  },
});
