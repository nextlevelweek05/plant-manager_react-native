import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';
import { content, title } from '../../../styles/typography';

export default StyleSheet.create({
  container: {
    backgroundColor: colors.shape,
    width: 76,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginHorizontal: 5,
  },
  containerActive: {
    backgroundColor: colors.green_light,
  },
  text: {
    ...content,
  },
  textActive: {
    ...title,
    color: colors.green,
  },
});
