import { RectButtonProps } from 'react-native-gesture-handler';

export interface IEnvironmentButton extends RectButtonProps {
  title: string;
  active?: boolean;
}
