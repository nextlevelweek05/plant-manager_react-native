import React from 'react';
import { Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import style from './style';

import { IEnvironmentButton } from './interface';

const EnvironmentButton = ({
  title,
  active = false,
  ...rest
}: IEnvironmentButton) => {
  return (
    <RectButton
      style={[style.container, active && style.containerActive]}
      {...rest}
    >
      <Text style={[style.text, active && style.textActive]}>{title}</Text>
    </RectButton>
  );
};

export default EnvironmentButton;
